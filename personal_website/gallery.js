
let randomUser = "student:";

let posts = document.querySelectorAll(".post");
let likesLock = Array(posts.length).fill(false);

function randomNumberLikes() {
    for(let i = 0; i < posts.length; i++) {
        let rndNumber = Math.ceil(Math.random() * 100);
        posts.item(i).querySelector("#counterLikes").innerHTML = rndNumber;
    }
}

randomNumberLikes();

function addLike(event) {

    if(event.target.getAttribute("id") == "like") {
        let id = event.target.parentNode.parentNode.id;
        let currentLikeCount = parseInt(posts.item(id).querySelector("#counterLikes").innerHTML);

        if(!likesLock[id]) {
            posts.item(id).querySelector("#counterLikes").innerHTML = currentLikeCount + 1;
            likesLock[id] = true;
            posts.item(id).querySelector("#like").style.color = "#fb3958";
            posts.item(id).querySelector("#like").innerHTML="favorite"
        }
        else if(likesLock[id]) {
            posts.item(id).querySelector("#counterLikes").innerHTML = currentLikeCount - 1;
            likesLock[id] = false;
            posts.item(id).querySelector("#like").style.color = "white";
            posts.item(id).querySelector("#like").innerHTML="favorite_border"
        }
    }
}

function addComment(event) {

    if(event.target.getAttribute("id") == "postComment") {
        let id = event.target.parentNode.parentNode.id;
        let commentInput = posts.item(id).querySelector("#comment").value;
        let comments = posts.item(id).querySelector(".comments");
        let comment = document.createElement("p");

        comment.className = "commentStyle";
        comment.innerHTML = randomUser + " " + commentInput;
        comments.appendChild(comment);

        posts.item(id).querySelector("#comment").value = "";
    }
}

document.addEventListener("click", addLike, false);
document.addEventListener("click", addComment, false);
