
let contact = document.getElementById("contactID");
let about = document.getElementById("aboutID");
let gallery = document.getElementById("galleryID");
let blog = document.getElementById("blogID");
let goUp = document.getElementById("up");

function scrollToContact() {
    let contactElement = document.getElementById("contactSection");
    contactElement.scrollIntoView({behavior: "smooth"});
}

function scrollToAbout() {
    let aboutElement = document.getElementById("aboutSection");
    aboutElement.scrollIntoView({behavior: "smooth"});
}

function scrollToGallery() {
    let galleryElement = document.getElementById("gallerySection");
    galleryElement.scrollIntoView({behavior: "smooth"});
}

function scrollToBlog() {
    let blogElement = document.getElementById("blogSection");
    blogElement.scrollIntoView({behavior: "smooth"});
}

function scrollUp() {
    console.log("DA");
    let upElement = document.getElementById("here");
    upElement.scrollIntoView({behavior: "smooth"});
}

contact.addEventListener("click", scrollToContact, false);
about.addEventListener("click", scrollToAbout, false);
gallery.addEventListener("click", scrollToGallery, false);
blog.addEventListener("click", scrollToBlog, false);
goUp.addEventListener("click", scrollUp, false);


window.onscroll = function() {
    let theta = document.documentElement.scrollTop / 90 % Math.PI;
    document.getElementById('shuttleSize').style.transform ='rotate(' + theta + 'rad)';

    let winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    let height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    let scrolled = (winScroll / height) * 100;
    document.getElementById("progBar").style.width = scrolled + "%";
}