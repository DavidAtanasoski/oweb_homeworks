document.getElementById("title").value = "";
document.getElementById("bodyText").value = "";

let publishBtn = document.getElementById("publishBtn");
let blogPosts = document.getElementById("blogPosts");

function publishPost() {
    let title = document.getElementById("title").value;
    let body = document.getElementById("bodyText").value;

    if(title == "" || body == "") {
        alert("You don't have a title or body for your blogpost!");
    }
    else {
        let datetime = new Date().toLocaleString("en-GB");

        let outterDiv = document.createElement("div");
        outterDiv.setAttribute("id", "blogs");

        let secondDiv = document.createElement("div");
        let blogTitle = document.createElement("h2");
        let publishDate = document.createElement("SPAN");
        publishDate.setAttribute("class", "postTime");

        publishDate.innerHTML = datetime;
        blogTitle.innerHTML = title;
        
        secondDiv.appendChild(blogTitle);
        secondDiv.appendChild(publishDate);
        outterDiv.appendChild(secondDiv);

        let bodyDiv = document.createElement("div");
        bodyDiv.setAttribute("class", "body");
        bodyDiv.innerHTML = body;

        outterDiv.append(bodyDiv);

        blogPosts.appendChild(outterDiv);

        document.getElementById("title").value = "";
        document.getElementById("bodyText").value = "";
    }
}


publishBtn.addEventListener("click", publishPost, false);

