import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit{

  squares = Array(9).fill(null);
  player:string;
  playerState:boolean;
  winner:string;

  ngOnInit()
  {
    this.player = "X";
    this.playerState = true;
  }

  getPlayer()
  {
    return this.playerState ? "X" : "O";
  }

  checkWinner()
  {
    const winningCombinations = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];

    for(let combination of winningCombinations)
    {
      let firstSquare = combination[0];
      let secondSquare = combination[1];
      let thirdSquare = combination[2];

      if(this.squares[firstSquare] &&
            this.squares[firstSquare] === this.squares[secondSquare] && 
            this.squares[secondSquare] === this.squares[thirdSquare] &&
            this.squares[firstSquare] === this.squares[thirdSquare])
            {
              return true;
            }
    }

    return false;
  }

  selectedSquare(squareIndex:number)
  {
    if(this.squares[squareIndex] !== 'X' && this.squares[squareIndex] !== 'O' && !this.winner)
    {
      this.player = this.getPlayer();
      this.squares.splice(squareIndex, 1, this.player);
      this.playerState = !this.playerState;
  
      if(this.checkWinner())
      {
        this.winner = this.player;
      }
      else
      {
        let flag = true;
        for(let i = 0; i < this.squares.length; i++)
        {
          if(this.squares[i] === null)
          {
            flag = false;
            break;
          }
        }

        if(flag)
        {
          this.winner = "DRAW";
        }
      }
    }
  }
}
